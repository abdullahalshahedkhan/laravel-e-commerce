<?php

namespace App\Jobs;

// app/Jobs/SendNewProductNotification.php

namespace App\Jobs;

use App\Mail\NewProductNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNewProductNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productName;
    protected $price;
    protected $quantity;
    protected $email;

    public function __construct($productName, $price, $quantity, $email)
    {
        $this->productName = $productName;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->email = $email;
    }

    public function handle()
    {
        // Dispatch the email
        \Mail::to($this->email)->send(new NewProductNotification($this->productName, $this->price, $this->quantity));
    }
}


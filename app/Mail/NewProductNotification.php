<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewProductNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $productName, $price, $quantity;

    public function __construct($productName, $price, $quantity)
    {
        $this->productName = $productName;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    public function build()
    {
        return $this->subject('New Product Notification')
                    ->view('emails.new_product_notification');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Events\ProductPurchased;
use App\Http\Controllers\Controller;
use App\Jobs\SendNewProductNotification;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::all(); // Fetch categories
        return view('products.create', compact('categories'));
    }

    public function store(Request $request)
    {
        // Validation logic here

        $product = Product::create($request->all());
        // Replace 'user@example.com' with the actual user's email
        $userEmail = 'alshahed.cse@gmail.com';

        // Dispatch the job to send the email
        // Dispatch the job to send the email
        SendNewProductNotification::dispatch($product->name, $product->price, $product->quantity, $userEmail);

        return redirect()->route('products.index');
    }

    public function edit(Product $product)
    {
        $categories = Category::all(); // Fetch categories
        return view('products.edit', compact('product', 'categories'));
    }

    public function update(Request $request, Product $product)
    {
        // Validation logic here

        $product->update($request->all());

        return redirect()->route('products.index');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }

    public function purchaseProduct($productId)
    {
        // Logic to handle the purchase
        $product = Product::find($productId);

        // Fire the ProductPurchased event
        event(new ProductPurchased($product));

        // Additional logic for the purchase
        // ...
    }
}

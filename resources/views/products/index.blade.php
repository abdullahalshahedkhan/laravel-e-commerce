<!-- resources/views/products/index.blade.php -->

@extends('backend.master')

@section('content')
<h2>Product List</h2>

<a href="{{ route('products.create') }}" class="btn btn-lg ot-btn-primary">Create Product</a>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered role-table">
            <thead>
                <tr>
                    <th class="serial">ID</th>
                    <th class="purchase">Name</th>
                    <th class="purchase">Price</th>
                    <th class="purchase">Quantity</th>
                    <th class="purchase">Category</th>
                    <th class="purchase">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($products as $product)
                <tr>
                    <td class="serial">{{ $product->id }}</td>
                    <td class="purchase">{{ $product->name }}</td>
                    <td class="purchase">{{ $product->price }}</td>
                    <td class="purchase">{{ $product->quantity }}</td>
                    <td class="purchase">{{ $product->category->name }}</td>
                    <td class="purchase">
                        <a class="fa-solid fa-pen-to-square" href="{{ route('products.edit', $product->id) }}">Edit</a>
                        <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="fa-solid fa-pen-to-square" type="submit" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6">No products found</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
<!-- resources/views/products/edit.blade.php -->

@extends('backend.master')

@section('content')
    <div class="card ot-card">
        <h2>Edit Product</h2>
        <div class="card-body">
            <form action="{{ route('products.update', $product->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="name">Name:</label>
                                <input class="form-control ot-input" type="text" name="name" value="{{ $product->name }}" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="price">Price:</label>
                                <input class="form-control ot-input" type="text" name="price" value="{{ $product->price }}" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="quantity">Quantity:</label>
                                <input class="form-control ot-input" type="text" name="quantity" value="{{ $product->quantity }}" required>
                            </div>
                            <div class="col-md-12">
                                <label for="category_id">Category:</label>
                                <select class="form-select ot-input" name="category_id" required>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $product->category_id == $category->id ? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- Add other fields as needed -->
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

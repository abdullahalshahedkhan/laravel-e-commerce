<!-- resources/views/emails/new_product_notification.blade.php -->

<p>Hello,</p>

<p>A new product Named "{{ $productName }},Price:-{{$price}},Quantity:-{{$quantity}}" has been added to our store.</p>

<p>Thank you!</p>

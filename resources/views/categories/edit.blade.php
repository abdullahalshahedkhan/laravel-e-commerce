<!-- resources/views/products/edit.blade.php -->

@extends('backend.master')

@section('content')
<div class="card ot-card">
    <h2>Edit Product</h2>
    <div class="card-body">
        <form action="{{ route('categories.update', $category->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row mb-3">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="name">Name:</label>
                            <input class="form-control ot-input" type="text" name="name" value="{{ $category->name }}" required>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>
        </form>
    </div>
</div>
@endsection
<!-- resources/views/products/index.blade.php -->

@extends('backend.master')


@section('content')
<h2>Categories List</h2>

<a href="{{ route('categories.create') }}" class="btn btn-lg ot-btn-primary">Create categories</a>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered role-table">
            <thead>
                <tr>
                    <th class="serial">ID</th>
                    <th class="purchase">Name</th>
                    <th class="purchase">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($categories as $category)
                <tr>
                    <td class="serial">{{ $category->id }}</td>
                    <td class="purchase">{{ $category->name }}</td>
                    <td class="purchase">
                        <a class="fa-solid fa-pen-to-square" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                        <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="fa-solid fa-pen-to-square" type="submit" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6">No products found</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
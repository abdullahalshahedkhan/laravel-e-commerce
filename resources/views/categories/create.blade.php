<!-- resources/views/products/create.blade.php -->

@extends('backend.master')

@section('title', __('common.add_new'))

@section('content')
    <div class="page-content">
        <div class="card ot-card border-0 ph-14 pv-14 mb-24">
            <div class="card-body pt-0">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb ot-breadcrumb-secondary mb-0">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ __('common.home') }}</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('categories.index') }}">{{ __('common.categories') }}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{ __('common.add_new') }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card ot-card">
            <div class="card-body">
                <form action="{{ route('products.store') }}" enctype="multipart/form-data" method="post"
                    id="productForm">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">{{ __('common.name') }} <span
                                            class="fillable">*</span></label>
                                    <input class="form-control ot-input @error('name') is-invalid @enderror" name="name"
                                        id="name" placeholder="{{ __('common.enter_name') }}" required>
                                    @error('name')
                                        <div id="validationServer04Feedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <!-- Add other fields as needed -->
                            </div>
                        </div>
                        <div class="col-md-12 mt-24">
                            <div class="text-end">
                                <button class="btn btn-lg ot-btn-primary"><span><i class="fa-solid fa-save"></i>
                                    </span>{{ __('common.submit') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
